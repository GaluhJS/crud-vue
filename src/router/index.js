import Vue from 'vue'
import Router from 'vue-router'

import ListCustomer from '@/pages/ListCustomer'
import AddCustomer from '@/pages/AddCustomer'
import EditCustomer from '@/pages/EditCustomer'

Vue.use(Router)

let router =  new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: ListCustomer,
    },
    {
      path: '/add',
      component: AddCustomer,
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: EditCustomer,
    },
  ]
})

export default router
